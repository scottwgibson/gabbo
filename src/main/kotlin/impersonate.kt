import com.ullink.slack.simpleslackapi.SlackAttachment
import com.ullink.slack.simpleslackapi.SlackSession
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener
import kotlinx.serialization.Serializable


typealias WordId = Int
typealias PairId = Int
typealias WordPair = Pair<WordId, WordId>
typealias PairChain = HashMap<WordPair, MutableSet<WordPair>>

@Serializable
class Impersonate : SlackMessagePostedListener {
    private var wordMap = HashMap<WordId, String>()
    private var chainMap = HashMap<String, PairChain>()


    override fun onEvent(event: SlackMessagePosted, session: SlackSession) {
        if (event.sender.id == session.sessionPersona().id) return // Don't do anything with our own messages.

        if (!event.sender.isBot) learn(event.messageContent, event.sender.id) //Don't imitate other bots.

        if (!event.messageContent.contains("<@${session.sessionPersona().id}>")) return // Only respond when asked

        val key = chainMap.keys.random()
        session.findUserById(key).let {user ->
            val userTitle = listOfNotNull(user.userTitle, user.realName).random()
            session.sendMessage(event.channel, "_imitating $userTitle:_ ${response(key)}")
        }
    }

    private fun learn(message: String, id: String) {
        var words = message.split(' ')
        words.forEach { this.wordMap.putIfAbsent(it.hashCode(), it) }
        var pairs = words.mapIndexed { i, t ->
            if (i == words.lastIndex) Pair(t.hashCode(), 0) else Pair(t.hashCode(), words[i + 1].hashCode())
        }

        chainMap.getOrPut(id, { PairChain() }).let { map ->
            pairs.iteratePairs { a, b -> map.getOrPut(a, { mutableSetOf() }).add(b) }
        }
    }

    private fun response(user: String): String {
        val userChain = chainMap.getOrElse(user, { return "error" })
        var pair = userChain.keys.random()
        return response(pair, userChain, "")
    }

    private tailrec fun response(
        pair: WordPair,
        userChain: PairChain,
        response: String
    ): String {
        val new_response = response + "${wordMap[pair.first]} "
        if (pair.second == 0)
            return new_response
        else {
            val next = userChain[pair]?.random() ?: Pair(pair.second, 0)
            return response(next, userChain, new_response)
        }
    }
}

fun <T> List<T>.iteratePairs(handle:(first: T, second: T) -> Unit )
{
    this.forEachIndexed { i, x -> if ( i == this.lastIndex ) return else handle(x, this[i+1]) }
}
