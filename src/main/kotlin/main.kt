import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory

fun main (args: Array<String>) {
    if ( args.isEmpty() ) { println("Need slack token."); return }

    val session = SlackSessionFactory.createWebSocketSlackSession(args[0])
    session.connect()
    Gabbo().registerSession(session)
}