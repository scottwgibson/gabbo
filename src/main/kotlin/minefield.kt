import com.ullink.slack.simpleslackapi.SlackSession
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener

class MineField : SlackMessagePostedListener {
    private val channelMap = HashMap<String, Boolean>()

    override fun onEvent(event: SlackMessagePosted, session: SlackSession) {
        if (event.sender.id == session.sessionPersona().id) return // Don't do anything with our own messages.

        if (event.messageContent.contains(Regex("\\bmine\\b")) && channelMap.getOrDefault(event.channel.id, false)){
            session.addReactionToMessage(event.channel, event.timeStamp, "mine")
        }

        channelMap[event.channel.id] = event.messageContent.trim().endsWith("?")
    }
}
