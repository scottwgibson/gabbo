import com.ullink.slack.simpleslackapi.SlackSession
import kotlinx.serialization.json.Json
import java.io.File

val IMPERSONATE_FILE = "impersonate.json"

class Gabbo {

    private val impersonate: Impersonate
    init {
        val file = File(IMPERSONATE_FILE)
        impersonate = if (file.exists()) Json.parse(Impersonate.serializer(), file.readText()) else Impersonate()
    }

    fun registerSession ( session: SlackSession ) {
        session.addMessagePostedListener(this.impersonate)
        session.addMessagePostedListener { event, _ ->
            if (!event.sender.isBot)
                File(IMPERSONATE_FILE).writeText(Json.stringify(Impersonate.serializer(), impersonate))
        }
        session.addMessagePostedListener(MineField())
    }
}

